package com.example.firebaseservices

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit

class LoginActivity : AppCompatActivity() {

    private var username: EditText? = null
    private var password: EditText? = null
    private var login: Button? = null
    private var registerUser: Button? = null
    private lateinit var auth: FirebaseAuth
    private lateinit var phone: EditText
    private lateinit var authButton: Button

    lateinit var storedVerificationId: String
    lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initViews()

        registerUser?.setOnClickListener {
            var goCreateAccount = Intent(applicationContext, CreateAccount::class.java)
            startActivity(goCreateAccount)

        }
        login?.setOnClickListener {
            var muser = username?.text.toString()
            var mpass = password?.text.toString()

            if (muser.isEmpty()) {
                username?.setError("Enter Username")
            } else if (mpass.isEmpty()) {
                password?.setError("Enter Password")
            } else {
                auth.signInWithEmailAndPassword(muser, mpass)
                    .addOnCompleteListener(this) {
                        if (it.isSuccessful) {
                            Toast.makeText(
                                applicationContext,
                                "Login Successed",
                                Toast.LENGTH_SHORT
                            ).show()
                            var goHome = Intent(applicationContext, HomeActivity::class.java)
                            goHome.putExtra("username", muser)
                            startActivity(goHome)

                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Invalid Credentials",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }
        }

        var currentUser = auth.currentUser
        if (currentUser != null) {
            startActivity(Intent(applicationContext, HomeActivity::class.java))
            finish()
        }
        authButton.setOnClickListener {
            var mPhone = phone.text.toString()

            if (mPhone.isEmpty()) {
                Toast.makeText(applicationContext, "Enter Phone Number", Toast.LENGTH_SHORT)
                    .show()
            } else if (mPhone.length < 10 || mPhone.length > 10) {
                Toast.makeText(applicationContext, "Enter Valid Phone Number", Toast.LENGTH_SHORT)
                    .show()
            } else {
                loginUser()
            }
        }

        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                startActivity(Intent(applicationContext, HomeActivity::class.java))
                finish()
            }

            override fun onVerificationFailed(e: FirebaseException) {
                Toast.makeText(applicationContext, "Failed", Toast.LENGTH_LONG).show()
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {

                Log.d("TAG", "onCodeSent:$verificationId")
                storedVerificationId = verificationId
                resendToken = token

                var intent = Intent(applicationContext, Verify::class.java)
                intent.putExtra("storedVerificationId", storedVerificationId)
                startActivity(intent)
            }
        }

    }

    private fun loginUser() {
        val mobileNumber = findViewById<EditText>(R.id.phoneno)
        var number = mobileNumber.text.toString().trim()

        if (!number.isEmpty()) {
            number = "+91" + number
            sendVerificationcode(number)
        } else {
            Toast.makeText(this, "Enter mobile number", Toast.LENGTH_SHORT).show()
        }


    }

    private fun sendVerificationcode(number: String) {
        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(number)
            .setTimeout(60L, TimeUnit.SECONDS)
            .setActivity(this)
            .setCallbacks(callbacks)
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)

    }

    private fun initViews() {
        username = findViewById(R.id.username)
        password = findViewById(R.id.password)
        login = findViewById(R.id.login)
        registerUser = findViewById(R.id.registeruser)
        auth = FirebaseAuth.getInstance()
        authButton = findViewById(R.id.authPhone)
        phone = findViewById(R.id.phoneno)

        val mobLogin = findViewById<Button>(R.id.authPhone)


    }
}