package com.example.firebaseservices

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth

class CreateAccount : AppCompatActivity() {

    private lateinit var email: EditText
    private lateinit var password: EditText
    private lateinit var repassword: EditText
    private lateinit var createAccount: Button
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)
        initViews()
        createAccount.setOnClickListener {
            signIn()
        }
    }

    private fun signIn() {
        var view: View
        view = findViewById(android.R.id.content)
        var musername = email.text.toString()
        var mpassword = password.text.toString()
        var mrepassword = repassword.text.toString()

        if (musername.isEmpty() || mpassword.isEmpty() || mrepassword.isEmpty()) {
            Snackbar.make(view, "All Fields are Mandatoy", Snackbar.LENGTH_SHORT).show()
        } else if (!Patterns.EMAIL_ADDRESS.matcher(musername).matches()) {

            Toast.makeText(applicationContext, "Enter valid Email Address", Toast.LENGTH_SHORT)
                .show()
        } else if (mpassword.length < 6 || mrepassword.length < 6) {
            Toast.makeText(applicationContext, "Password must min. 6 digits", Toast.LENGTH_SHORT)
                .show()
        } else if (!mpassword.equals(mrepassword)) {
            Toast.makeText(applicationContext, "Password Mismatches", Toast.LENGTH_SHORT).show()
        } else {

            auth.createUserWithEmailAndPassword(musername, mpassword)
                .addOnCompleteListener(this) {
                    if (it.isSuccessful) {
                        Toast.makeText(
                            applicationContext,
                            "Account Creation Successed",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        var gotoLogin = Intent(this, LoginActivity::class.java)
                        startActivity(gotoLogin)
                        email.text = null
                        password.text = null
                        repassword.text = null

                    } else {
                        Toast.makeText(
                            applicationContext,
                            "Account Creation Failed..",
                            Toast.LENGTH_SHORT
                        )
                            .show()

                    }
                }
        }
    }


    private fun initViews() {

        email = findViewById(R.id.email)
        password = findViewById(R.id.password)
        repassword = findViewById(R.id.repassword)
        createAccount = findViewById(R.id.create_account)
        auth = FirebaseAuth.getInstance()
    }
}