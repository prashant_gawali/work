package com.example.firebaseservices

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth

class HomeActivity : AppCompatActivity() {

    private var logout: Button? = null
    private lateinit var auth: FirebaseAuth
    private lateinit var uid: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        logout = findViewById(R.id.logout)
        auth = FirebaseAuth.getInstance()
        uid = findViewById(R.id.uid)

        var ss: String = intent.getStringExtra("username").toString()
        if (!ss.isEmpty()) {

            uid.setText(ss)

        } else {

            uid.text = "Logged In using Phone Number"

        }


        logout?.setOnClickListener {
            auth.signOut()
            var goLogin = Intent(applicationContext, LoginActivity::class.java)
            startActivity(goLogin)

        }

        auth = FirebaseAuth.getInstance()
        var currentUser = auth.currentUser

        if (currentUser == null) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

    }
}